resource "aws_api_gateway_method" "main" {
  rest_api_id   = var.api_id
  resource_id   = var.resource_id
  authorization_scopes = var.authorization_scopes
  http_method   = var.http_method
  authorization = var.authorization
  request_models = var.request_models
  request_parameters = var.request_parameters
  request_validator_id = var.request_validator_id
  api_key_required = var.api_key_required
}

resource "aws_api_gateway_method_response" "main" {
  rest_api_id = var.api_id
  resource_id = var.resource_id
  http_method = aws_api_gateway_method.main.http_method
  
  for_each = var.method_responses
  status_code = each.key
  response_models = each.value.response_models
  response_parameters = each.value.response_parameters
}
