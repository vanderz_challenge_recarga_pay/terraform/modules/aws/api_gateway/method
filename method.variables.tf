variable "api_id" {}

variable "resource_id" {}

variable "http_method" {}

variable "authorization" {}

variable "authorization_scopes" {}

variable "request_parameters" {}

variable "request_models" {}

variable "request_validator_id" {}

variable "api_key_required" {}

variable "method_responses" {}
